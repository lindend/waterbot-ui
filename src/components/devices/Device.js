import React from 'react';
import { firebase } from '../../firebase';

import DeviceStats from './DeviceStats';

const InitialState = {
    device: null
};

class DevicePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = { ...InitialState };
        this.deviceId = this.props.match.params.id;
    }

    componentDidMount() {
        
        firebase.db.collection('devices')
            .doc(this.deviceId)
            .get()
            .then(device => {
                this.setState({device: device.data()});
            })
            .catch((error) => alert(error));
    }

    render() {
        return (
            <div>
                <div>
                    { 
                        this.state.device ? 
                            DeviceInfo(this.state.device) :
                            <div>Loading</div>
                    }
                </div>
                <DeviceStats deviceId={this.deviceId} />
            </div>
        );
    }
}

function DeviceInfo(device) {
    return (
        <div>
            <h1>{device.name}</h1>
            <h2>Sensors</h2>
            {Object.keys(device.sensors).map(key => <div key={key}>{key}</div>)}

            <h2>Agents</h2>
            {Object.keys(device.agents).map(key => <div key={key}>{key}</div>)}

            <h2>Channels</h2>
            {Object.keys(device.channels).map(key => <div key={key}>{key}</div>)}
        </div>
    );
}

export default DevicePage;