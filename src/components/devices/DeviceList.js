import React from 'react';
import { firebase } from '../../firebase';
import { routes } from '../../constants';
import { Link } from 'react-router-dom';

const InitialState = {
    devices: [],
    isLoading: true,
};

class DeviceList extends React.Component {
    constructor(props) {
        super(props);

        this.userId = props.userId;

        this.state = { ...InitialState };
    }

    componentDidMount() {
        firebase.db.collection('devices')
            .where('userId', '==', this.userId)
            .get()
            .then(querySnapshot => {
                const devices = querySnapshot.docs.map(doc => { 
                    return {
                        id: doc.id,
                        ...doc.data()
                    };
                });
                this.setState({devices: devices});
            })
            .catch(error => {
                alert(error);
            });
    }

    render() {
        return (
            <ul>
                { this.state.devices.map(DeviceListDevice) }
            </ul>
        );
    }
}

function DeviceListDevice(device) {
    return (
        <li>
            <Link to={routes.DEVICE.replace(':id', device.id)}>{device.name}</Link>
        </li>
    );
}

export default DeviceList;