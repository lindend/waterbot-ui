import React from 'react';
import AuthUserContext from '../AuthUserContext';
import DeviceList from './DeviceList';

function DevicesPage() {
    return (
        <div>
            <AuthUserContext.Consumer>
                { authUser => 
                    authUser ?
                    <DeviceList userId={authUser.uid} /> :
                    <span> Unauthorized </span>
                }
            </AuthUserContext.Consumer>
        </div>
    );
}

export default DevicesPage;