import React from 'react';
import { firebase } from '../../firebase';
import * as d3 from 'd3';
import './DeviceStats.css';

const now = new Date();

const InitialState = {
    isLoading: false,
    currentStats: null,
    selectedYear: now.getFullYear(),
    selectedMonth: now.getMonth() + 1,
};

function parseDeviceStatsData(data) {
    for (let key of Object.keys(data)) {
        const sensor = data[key];
        for (let value of sensor.values) {
            if (typeof value.t.toDate === 'function') {
                value.t = value.t.toDate();
            }
            else if (!(value.t instanceof Date)) {
                value.t = new Date(value.t);
            }
        }
    }
    return data;
}

class DeviceStats extends React.Component {
    constructor(props) {
        super(props);
        this.state = { ...InitialState };
    }

    componentDidMount() {
        this.deviceId = this.props.deviceId;
        this.loadStats();
    }

    loadStats() {
        this.setState({isLoading: true});
        firebase.db.collection('devices')
            .doc(this.deviceId)
            .collection('stats')
            .doc(`${this.state.selectedYear}-${this.state.selectedMonth}`)
            .get()
            .then(stats => {
                this.setState({
                    currentStats: parseDeviceStatsData(stats.data()), 
                    isLoading: false
                });
            })
            .catch((error) => alert(error));
    }

    render() {
        return (
            <div>
                { 
                    this.state.currentStats ? 
                        DeviceStatsGraph(this.state.currentStats) :
                        <div>Loading</div>
                }
            </div>
        );
    }
}

function DeviceStatsGraph(deviceStats) {
    const width = 400;
    const height = 400;

    const statsSources = Object.keys(deviceStats).map(key => deviceStats[key]);
    const timeRanges = statsSources.map(stats => d3.extent(stats.values, v => v.t))
        .reduce((a1, a2) => a1.concat(a2), []);
    const xScale = d3.scaleTime()
        .domain(d3.extent(timeRanges))
        .range([0, width]);


    return (
        <div>
            <h2>Stuff</h2>
            {Object.keys(deviceStats).map(key => <div key={key}>{key}</div>)}

            <svg width={width} height={height} className="DeviceStats">
                {Object.keys(deviceStats).map(key => <DeviceStatsLine key={key} stats={deviceStats[key]} xScale={xScale} />)}
            </svg>
        </div>
    );
}

function DrawLines(stats, xScale, height, lineDefinitions) {
    const transformX = data => xScale(data.t);

    const lines = lineDefinitions.map(lineDefinition => {
        const selector = lineDefinition.selector;
        const extent = d3.extent(stats.values, v => selector(v));
        const yScale = d3.scaleLinear()
            .domain(extent)
            .range([height, 0]);

        const transformY = data => yScale(selector(data));
        const lineDrawer = d3.line()
            .curve(d3.curveCardinal)
            .x(transformX)
            .y(transformY);

        const filteredValues = stats.values.filter(v => selector(v) !== undefined);

        return {
            ...lineDefinition,
            path: lineDrawer(filteredValues),
        };
    });

    return (
        <g className="line">
            { lines.map((line, i) => <path key={i} d={line.path} className={line.class}/>) }
        </g>
    );
}

function DrawEvents(stats, xScale, height, eventDefinition) {
    const radius = 2;
    const circle = (t) => <circle key={t} cx={xScale(t)} cy={height - 10} r={radius} className={eventDefinition.class} />
    return (
        <g>
            {stats.values.map(v => circle(v.t))}
        </g>
    );
}

function DeviceStatsLine(props) {
    const height = 400;
    const stats = props.stats;
    const xScale = props.xScale;

    if (stats.type === 'moisture') {
        return DrawLines(stats, xScale, height, [ {
            selector: value => value.v,
            class: 'moisture'
        }]);
    } else if (stats.type === 'dht11') {
        return DrawLines(stats, xScale, height, [ {
            selector: value => value.v.t,
            class: 'temperature',
        }, {
            selector: value => value.v.h,
            class: 'humidity',
        }]);
    } else if (stats.type === 'event') {
        return DrawEvents(stats, xScale, height, {
                class: 'event'
            });
    }

    return null;
}

export default DeviceStats;