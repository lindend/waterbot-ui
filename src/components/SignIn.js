import React from 'react';
import { withRouter } from 'react-router-dom';

import { auth } from '../firebase';
import { routes } from '../constants';

import { SignUpLink } from './SignUp';

const SignInPage = ({ history }) =>
    <div>
        <h1>Sign in</h1>
        <SignInForm history={history} />
        <SignUpLink />
    </div>

const InitialState = {
    email: '',
    password: '',
    error: null
};

class SignInForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = { ...InitialState };
    }

    onSubmit = (event) => {
        const {
            email,
            password,
        } = this.state;

        const {
            history
        } = this.props;

        auth.signInWithEmailAndPassword(email, password)
            .then(() => {
                this.setState({ ...InitialState });
                history.push(routes.HOME);
            })
            .catch(err => {
                this.setState({ error: err });
            });
        event.preventDefault();
    }

    render() {
        const {
            email,
            password,
            error,
        } = this.state;

        const isInvalid = 
            password === '' ||
            email === '';

        return (
            <form onSubmit={this.onSubmit}>
                <input
                    value={email}
                    onChange={event => this.setState({email: event.target.value})}
                    type="text"
                    placeholder="Email address"
                />
                <input
                    value={password}
                    onChange={event => this.setState({password: event.target.value})}
                    type="password"
                    placeholder="Password"
                />
                <button disabled={isInvalid} type="submit">
                    Sign in
                </button>

                { error && <p>{error.message}</p> }
            </form>
        );
    }
}

export default withRouter(SignInPage);