import React from 'react';

const Error = (props) => (
    <div>
        {
            props.message ?
                (<h1>OMG ERROR {props.message}</h1>) :
                <div />
        }
    </div>
)