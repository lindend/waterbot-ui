import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

const config = {
    apiKey: "AIzaSyAx3uxQ3d_lxfh-46axWNqUnfDbbYpUDHk",
    authDomain: "waterbot-1.firebaseapp.com",
    databaseURL: "https://waterbot-1.firebaseio.com",
    projectId: "waterbot-1",
    storageBucket: "waterbot-1.appspot.com",
    messagingSenderId: "666967857770"
};

if (!firebase.apps.length) {
    firebase.initializeApp(config);
}

const auth = firebase.auth();
const db = firebase.firestore();
const settings = { timestampsInSnapshots: true };
db.settings(settings);

export {
    auth,
    db
};